﻿using UnityEngine;

public class SwordDestroyer : MonoBehaviour {

    private void OnTriggerEnter2D(Collider2D other)
    {
        if(other.CompareTag("Trash")){
            // Some action here
            Vector2 direction = other.transform.position - transform.position;
            direction.Normalize();

            other.transform.position += (Vector3)direction * 15;
            other.GetComponent<TrashStuff>().RecieveDamage(1);
        }
		else if (other.CompareTag("Hands"))
		{
			Vector2 direction = other.transform.position - transform.position;
			direction.Normalize();

            other.GetComponentInParent<Rigidbody2D>().AddForce(direction * 150, ForceMode2D.Impulse);
        }
    }

    public void onAnimationEnd()
    {
        Destroy(gameObject);
    }
}
