﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CameraFollower : MonoBehaviour {

    public static CameraFollower mainCam;
    public static event Action onReadyEvent;

    public static Transform PersonToFollow;
    public float FollowSpeedY = 0.35f;
    public float FollowSpeedX = 0.25f;
    public Vector2 CameraOffset;
    private InputState heroState;
    private Rigidbody2D rBody;

    private SpriteRenderer background;

    public Vector3 BossCameraPosition;

    public bool BossFight;

    public float Amp = 1;
    public int Length = 60;

    public Image TransitionImage;
    public int TransitionDuration = 60;

    public bool ReadyToFollow = false;



    private void Start()
    {
        mainCam = this;

        background = GetComponentInChildren<SpriteRenderer>();
    }

    public void SetPersonToFollow(Transform person)
    {
        PersonToFollow = person;
        heroState = person.GetComponentInChildren<InputState>();
        ReadyToFollow = true;
    }

    private void FixedUpdate()
	{
        if (PersonToFollow == null)
            ReadyToFollow = false;

		if (ReadyToFollow)
            Follow();
    }

    private void Follow()
    {
        if (!BossFight)
        {
            float targetX = Mathf.Lerp(transform.position.x, PersonToFollow.position.x + (float)heroState.direction * CameraOffset.x, FollowSpeedX);
            float targetY = Mathf.Lerp(transform.position.y, PersonToFollow.position.y + CameraOffset.y, FollowSpeedY);

            transform.position = Vector3.Lerp(transform.position, new Vector3(targetX, targetY) + new Vector3(0, 0, -10), FollowSpeedY);
        }
        else{
            transform.position = Vector3.Lerp(transform.position, BossCameraPosition, FollowSpeedY);
        }
    }

    [ContextMenu("screenshake")]
    public void TEST(){
        ScreenShake(Amp);        
    }

    public void ScreenShake(float amplitude){
        StopCoroutine(CoroutineScreenShake(amplitude * Amp));
        StartCoroutine(CoroutineScreenShake(amplitude * Amp));
        
    }
    private void OnDrawGizmosSelected()
    {
        Gizmos.color = Color.green;
        Gizmos.DrawWireCube(BossCameraPosition, new Vector3(1929, 1080));
    }

    IEnumerator CoroutineScreenShake(float amplitude){

        for (int i = 0; i < Length; i++)
        {

            transform.position += new Vector3(UnityEngine.Random.Range(-amplitude, amplitude), UnityEngine.Random.Range(-amplitude, amplitude));
            yield return new WaitForEndOfFrame();
        }
    }

    public void TransitionStuff()
    {
        StartCoroutine(Transition());
	}

	public void TransitionStuffback()
	{
		StartCoroutine(TransitionBack());
	}

    public void Zoomout() 
    {
        StartCoroutine(ZoomOut());
    }

    IEnumerator ZoomOut()
    {
        Camera cam = GetComponent<Camera>();
        FollowSpeedX = 0.35f * 0.05f;
        FollowSpeedY = 0.25f * 0.05f;
        for (int i = 0; i < 240; i++)
        {
            if(cam.orthographicSize < 160){
                cam.orthographicSize += .75f;
            }
            yield return new WaitForEndOfFrame();
		}
		FollowSpeedX = 0.35f * 1;
		FollowSpeedY = 0.25f * 1;

        cam.orthographicSize = 160;
    }

    IEnumerator Transition()
    {
        BossFight = false;
        Camera cam = GetComponent<Camera>();

        for (int i = 0; i < TransitionDuration; i++)
        {
            if (cam.orthographicSize > 80)
                cam.orthographicSize -= 2;

            Color col = TransitionImage.color;
            col.a += Time.deltaTime;
            TransitionImage.color = col;
            yield return new WaitForEndOfFrame();
        }

        onReadyEvent?.Invoke();

        cam.orthographicSize = 80;
    }

    IEnumerator TransitionBack(){
        for (int i = 0; i < TransitionDuration; i++)
        {
            Color col = TransitionImage.color;
            col.a -= Time.deltaTime;
            TransitionImage.color = col;
            yield return new WaitForEndOfFrame();
        }
    }
}
