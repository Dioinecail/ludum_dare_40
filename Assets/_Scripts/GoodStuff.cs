﻿using UnityEngine;

public class GoodStuff : MonoBehaviour {

    public float KnowledgeBonus = 2;

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.CompareTag("Player"))
        {
            collision.GetComponent<PlayerControl>().onKnowledgeChange(KnowledgeBonus);
            Destroy(gameObject);
        }
    }
}
