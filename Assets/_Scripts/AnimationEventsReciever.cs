﻿using UnityEngine;

public class AnimationEventsReciever : MonoBehaviour {

    private AnimationManager animationManager;
    private PlayerControl Player;

    private void Start()
    {
        Player = GetComponentInParent<PlayerControl>();
        animationManager = GetComponentInParent<AnimationManager>();
    }

    public void onStrikeAnimationEvent()
    {
        Player.Strike();
    }

    public void onStepEvent(){
        Player.StepEvent();
    }

    public void onJumpEvent(){
        Player.JumpEvent();
    }

}
