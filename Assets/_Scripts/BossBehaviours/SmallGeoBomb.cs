﻿using UnityEngine;

public class SmallGeoBomb : MonoBehaviour {

    public float Damage = 0.1f;

    private void OnCollisionEnter2D(Collision2D other)
    {
        if(other.collider.CompareTag("Player")){
            other.collider.GetComponent<PlayerControl>().onKnowledgeChange(-Damage);
        }
    }
}
