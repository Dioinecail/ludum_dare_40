﻿using System.Collections;
using UnityEngine;

public class ChemistryBomb : BombMechanic {

    public Vector2 WarpOffest;
    public float ThrowForce = 150;

    protected override void Start()
    {
        base.Start();
        OnDetonate();
    }

    private void OnDrawGizmosSelected()
    {
        Gizmos.color = Color.cyan;
        Gizmos.DrawWireSphere((Vector2)transform.position + WarpOffest, 15);
    }

    protected override void OnDetonate()
    {
        StartCoroutine(Warp());
        base.OnDetonate();
    }

    IEnumerator Warp(){
        Vector2 warpPosition = new Vector2(Target.position.x < 600 ? WarpOffest.x : Random.Range(0, 2) == 1 ? WarpOffest.x : -WarpOffest.x, WarpOffest.y);
        transform.position = (Vector2)Target.position + warpPosition;

        yield return new WaitForEndOfFrame();

        Vector2 direction = Target.position - transform.position;
        direction.Normalize();

        bombBody.velocity = direction * ThrowForce;
    }
}
