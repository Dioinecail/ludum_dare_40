﻿using UnityEngine;

public class SmallMathBomb : MonoBehaviour {

    private Rigidbody2D bombBody;

    private Transform target;

    private CameraFollower cam;

    public float Speed = 100;
    public float TurnSpeed = 0.125f;
    public float Damage = 1;

    private void Start(){
        bombBody = GetComponent<Rigidbody2D>();
        target = FindObjectOfType<PlayerControl>().transform;
        cam = FindObjectOfType<CameraFollower>();

    }

    private void Update()
    {
        Vector2 directionToTurn = target.position - transform.position;
        directionToTurn.Normalize();

        Vector3 cross = Vector3.Cross(transform.up, directionToTurn);

        float angle = Vector2.Angle(transform.up, directionToTurn);

        if(cross.z < 0){
            angle = -angle;
        }

        Quaternion targetRotation = Quaternion.Euler(0, 0, angle) * transform.rotation;

        transform.rotation = Quaternion.Lerp(transform.rotation, targetRotation, TurnSpeed);
        bombBody.velocity = transform.up * Speed;
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        if(other.CompareTag("Player")){
            other.GetComponent<PlayerControl>().onKnowledgeChange(-Damage);
		}
        cam.ScreenShake(0.5f);
		Destroy(gameObject);
    }
}
