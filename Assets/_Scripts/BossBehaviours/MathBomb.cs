﻿using UnityEngine;

public class MathBomb : BombMechanic {

    public float InitialSpeed = 150;
    public GameObject SmallBomb;

    private Transform target;

    protected override void Start()
    {
        base.Start();
        ChangeDirection();
        target = FindObjectOfType<PlayerControl>().transform;

    }
    protected override void OnDetonate()
    {
        base.OnDetonate();
        Vector3 direction = target.position - transform.position;
        direction.Normalize();

        Instantiate(SmallBomb, transform.position + direction * 10, Quaternion.Euler(direction));
    }

    protected override void OnCollisionEnter2D(Collision2D collision)
    {
        if (!collision.collider.CompareTag("Player") && !onGround)
        {
            onGround = true;
            OnFall();
        }
        else if (!collision.collider.CompareTag("Player"))
        {
            ChangeDirection();
        }
    }

    void ChangeDirection()
    {
        Vector2 direction = new Vector2(Random.Range(-1, 1f), Random.Range(-1, 1f));
        direction.Normalize();

        bombBody.velocity = direction * InitialSpeed;
    }
}
