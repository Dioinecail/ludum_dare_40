﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class BossBehaviour : MonoBehaviour {

    public BossState state = BossState.NotInFight;
    public Animator Anima;
    [SerializeField]
    private bool Triggered = false;
    [SerializeField]
    private bool Talking = false;   
    public float TalkTimer = 3;
	[SerializeField]
	private float tlkTimer;
	[SerializeField]
	private bool Attacked = false;

    public GameObject Bomb;
    public GameObject BossTrigger;

    public Vector2 BombSpawnPoint = new Vector2(-130, 270);

    public new CameraFollower camera;

    public GameObject TalkCanvas;

    public Text QuestionText;
    public Text CorrectAnswer;
    public Text WrongAnswer;

    public Button CorrectButton;
    public Button WrongButton;

    [TextArea]
	public string[] Question;
	[TextArea]
	public string[] GoodReply;
	[TextArea]
	public string[] BadReply;
	[TextArea]
	public string[] CorrectAnswers;
	[TextArea]
	public string[] WrongAnswers;

    public int QuestionNumber = 0;

    public Vector2 PlayerRespawnPosition;
    public bool FightIsFinished;

    public GameObject NextBoss;

    public GameObject[] GoodStuffToAppear;
    public GameObject[] StuffToDiappear;

    private int AttacksCount;

    public static int BombsLeft;

    [ContextMenu("Trigger")]
    public void Trigger(){
        tlkTimer = TalkTimer;
        Anima.SetTrigger("Attack");
        Triggered = true;
        camera.BossFight = true;
        camera.Zoomout();

		CorrectButton.onClick.RemoveAllListeners();
		WrongButton.onClick.RemoveAllListeners();

        CorrectButton.onClick.AddListener(() => OnCorrectAnswer());
        CorrectButton.interactable = false;
        WrongButton.onClick.AddListener(() => OnWrongAnswer());
        WrongButton.interactable = false;

        MusicControl.Instance.SwitchMusic(1);
		FindObjectOfType<PlayerControl>().NoBoss = false;

        foreach (var trash in FindObjectsOfType<TrashStuff>())
        {
            Destroy(trash.gameObject);
        }
    }

    private void OnDrawGizmos()
    {
        Gizmos.color = Color.red;
        Gizmos.DrawWireSphere((Vector2)transform.position + BombSpawnPoint, 5);
        Gizmos.color = Color.cyan;
        Gizmos.DrawWireSphere(PlayerRespawnPosition, 10);
    }
    private void Update()
    {
        switch (state)
        {
			case BossState.NotInFight:
				if (Triggered)
				{

					if (tlkTimer > 0)
					{
						tlkTimer -= Time.deltaTime;
					}
					else
					{
						Anima.SetTrigger("Attack");
						tlkTimer = 15;
						state = BossState.Attack;
                        
                    }
                }
                break;
            case BossState.Idle:
                if(tlkTimer > 0){
                    tlkTimer -= Time.deltaTime;
                }
                else {
                    if (AttacksCount < 3)
                    {
                        AttacksCount++;
                        tlkTimer = 5;
                        Anima.SetTrigger("Attack");
                        state = BossState.Attack;
                    }
                    else {
						AttacksCount = 0;
						if (QuestionNumber < 3)
						{
							Talking = true;
							PassTexts();
							state = BossState.Talk;
						}
						else if (!FightIsFinished)
						{
                            // transition here
                            if (BombsLeft == 0)
                            {
                                FightIsFinished = true;
                                camera.TransitionStuff();
                                BossTrigger.SetActive(true);
                                CameraFollower.onReadyEvent += WarpPlayer;
                            }
						}
                    }
                }
                
                break;
            case BossState.Talk:
                if(!Talking){
                    Anima.SetTrigger("FinishedTalking");
                    state = BossState.Idle;
                }
                else {

					if (BombsLeft == 0)
					{

						Anima.SetTrigger("Talk");
						TalkCanvas.SetActive(true);
						// Freeze the player here
					}
                }
                
                break;
            case BossState.Attack:
                if(tlkTimer > 0){
                    if(Attacked)
                    tlkTimer -= Time.deltaTime;
                }
                else {
                    state = BossState.Idle;
                    tlkTimer = 1;
                }
                break;
            default:
                break;
        }
    }

    private void WarpPlayer(){
        FindObjectOfType<PlayerControl>().transform.position = PlayerRespawnPosition;
        FindObjectOfType<PlayerControl>().NoBoss = true;

        if (NextBoss != null)
        {
            NextBoss.SetActive(true);
            MusicControl.Instance.SwitchMusic(0);
        }
        else
        {
            MusicControl.Instance.SwitchMusic(2);
			// Send player to the last zone
			FindObjectOfType<PlayerControl>().NoBoss = false;
            FindObjectOfType<PlayerControl>().DressupNormal();
            FindObjectOfType<DashBehaviour>().enabled = false;
        }
        gameObject.SetActive(false);

		camera.TransitionStuffback();
        CameraFollower.onReadyEvent -= WarpPlayer;
    }
    private void PassTexts(){
        QuestionText.text = Question[QuestionNumber];
        CorrectAnswer.text = FindObjectOfType<PlayerControl>().AmountOfKnowledge / ((QuestionNumber + 1)*3) > 1 ? CorrectAnswers[QuestionNumber] : "I don't know";
        WrongAnswer.text = WrongAnswers[QuestionNumber];

        CorrectButton.interactable = (FindObjectOfType<PlayerControl>().AmountOfKnowledge / ((QuestionNumber + 1) * 3)) > 1;
        WrongButton.interactable = true;
    }
    public void OnCorrectAnswer(){
        if (Talking)
        {
            CorrectButton.interactable = false;
            WrongButton.interactable = false;
            QuestionText.text = GoodReply[QuestionNumber];
            QuestionNumber++;

            FindObjectOfType<PlayerControl>().RecieveCorrectAnswer();

            Talking = false;
            StartCoroutine(TalkWait(CorrectButton));
        }
        else
		{
            TalkCanvas.SetActive(false);
        }
    }
	public void OnWrongAnswer()
	{
        if (Talking)
        {
            CorrectButton.interactable = false;
            WrongButton.interactable = false;
            Talking = false;
            camera.ScreenShake(1);
            QuestionText.text = BadReply[QuestionNumber];
            QuestionNumber++;
			StartCoroutine(TalkWait(WrongButton));
			Instantiate(Bomb, (Vector2)transform.position + BombSpawnPoint, Quaternion.identity);
            BombsLeft++;
        }
        else
        {
			TalkCanvas.SetActive(false);
        }
    }

    public void onAttackAnimationEvent(){
        // spawn bombs
        if(state == BossState.Attack){
            camera.ScreenShake(1);
            Attacked = true;
            Instantiate(Bomb, (Vector2)transform.position + BombSpawnPoint, Quaternion.identity);
            BombsLeft++;
            // attack only if in attack state
        }
    }

    private void OnEnable()
    {
        for (int i = 0; i < GoodStuffToAppear.Length; i++)
        {
            GoodStuffToAppear[i].SetActive(true);
            if (StuffToDiappear != null)
                StuffToDiappear[i].SetActive(false);
        }
    }
    private IEnumerator TalkWait(Button switchButtonAfterWait){
        yield return new WaitForSecondsRealtime(1);
        switchButtonAfterWait.interactable = true;
    }
}

public enum BossState{
    NotInFight,
    Idle,
    Talk,
    Attack
}