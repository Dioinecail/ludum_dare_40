﻿using UnityEngine;

public class BossTrigger : MonoBehaviour {

    public GameObject Wall;
    public BossBehaviour[] Boss;
    public int currentBoss;

    private void OnEnable()
    {
        Wall.SetActive(false);
    }

    private void OnTriggerExit2D(Collider2D other)
    {
        if(other.CompareTag("Player")){
            // Trigger a boss
            if (other.transform.position.x > transform.position.x)
            {
                Boss[currentBoss].Trigger();
                Wall.SetActive(true);
                currentBoss++;
                gameObject.SetActive(false);
            }
        }
    }
}
