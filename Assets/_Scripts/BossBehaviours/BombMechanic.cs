﻿using System.Collections;
using UnityEngine;

[RequireComponent(typeof(Rigidbody2D))]
public class BombMechanic : MonoBehaviour {

    protected Rigidbody2D bombBody;
    protected Transform Target;

    protected CameraFollower cam;

    public float DestroyTimer = 5;

    public float ThrowTimer = 1;

    protected bool onGround;
    
    protected virtual void Start()
    {
        bombBody = GetComponent<Rigidbody2D>();
        Target = FindObjectOfType<PlayerControl>().transform;
        cam = FindObjectOfType<CameraFollower>();

    }

    protected virtual void OnCollisionEnter2D(Collision2D collision)
    {
        if (!collision.collider.CompareTag("Player") && !onGround)
        {
            onGround = true;
            OnFall();
        }
        else if(collision.collider.CompareTag("Player")){
            collision.collider.GetComponent<PlayerControl>().onKnowledgeChange(-0.1f);
        }
    }

    protected virtual void OnDestroy()
    {
        BossBehaviour.BombsLeft--;
    }

    protected virtual void OnFall()
    {
        StartCoroutine(ThrowStuff());
        Destroy(gameObject, DestroyTimer);
    }

    protected virtual void OnDetonate()
    {
        cam.ScreenShake(1);
    }

    protected virtual IEnumerator ThrowStuff()
    {
        OnDetonate();
           yield return new WaitForSecondsRealtime(ThrowTimer);
        StartCoroutine(ThrowStuff());
    }
}
