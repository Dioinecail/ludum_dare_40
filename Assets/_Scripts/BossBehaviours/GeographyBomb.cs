﻿using UnityEngine;
using System.Collections;

public class GeographyBomb : BombMechanic{
    public float TurnSpeed = 30;

    public float ThrowForce = 55;

    public GameObject SmallBomb;

    private void Update()
    {
        if (onGround)
        {
            bombBody.angularVelocity = TurnSpeed;
        }
    }

    protected override void Start()
    {
        base.Start();
        StartCoroutine(Jump());
    }
    protected override void OnDestroy()
    {
        base.OnDestroy();
        for (int i = 0; i < 5; i++)
        {
            Rigidbody2D smallBomb = Instantiate(SmallBomb, transform.position, Quaternion.identity).GetComponent<Rigidbody2D>();

            Vector2 direction = new Vector2( Random.Range(-1, 1f), 1);
            direction.Normalize();
            smallBomb.velocity = direction * ThrowForce;
            Destroy(smallBomb.gameObject, 3);
        }
    }

    protected override void OnDetonate()
    {
        base.OnDetonate();
        Rigidbody2D smallBomb = Instantiate(SmallBomb, transform.position, Quaternion.identity).GetComponent<Rigidbody2D>();

            Vector2 direction = Target.position - transform.position;
            direction.Normalize();
        smallBomb.velocity = direction * ThrowForce;
        Destroy(smallBomb.gameObject, 3);
    }

    protected IEnumerator Jump(){
        
        bombBody.velocity = new Vector2( Random.Range(-1, 1f), Random.Range(0, 1f)) * ThrowForce;

        yield return new WaitForSecondsRealtime(ThrowTimer);

        StartCoroutine(Jump());
    }
}
