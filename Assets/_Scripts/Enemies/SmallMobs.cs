﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SmallMobs : MonoBehaviour {

    public float Slowfactor = 2;
    public float Speed = 15;
    public float DisappearDistance = 200;

    public bool FaceRight = true;

    private Transform Player;

    private void Start()
    {
        LayerMask collisionLayer = LayerMask.GetMask("Solid");
        RaycastHit2D belowHit = Physics2D.Raycast(transform.position, Vector2.down, 50, collisionLayer);

        Player = FindObjectOfType<PlayerControl>().transform;


        if(belowHit.collider != null)
        {
            Vector2 bottomOfCollider = GetComponent<Collider2D>().bounds.min;
            Vector2 bottomToTransform = (Vector2)transform.position - new Vector2(transform.position.x, bottomOfCollider.y);

            transform.position = belowHit.point + bottomToTransform;
        }

    }

    private void Update()
    {
        transform.position += new Vector3((FaceRight ? Speed : -Speed) * Time.deltaTime, 0);
        if (Vector2.Distance(transform.position, Player.position) > DisappearDistance)
        {
            Destroy(gameObject);
        }
    }
    
    private void OnTriggerStay2D(Collider2D other)
    {
        if (other.CompareTag("Player"))
        {
            other.GetComponent<Walk>().SpeedMultiplier = 1 / Slowfactor;
            other.GetComponent<Walk>().ToggleScripts(false);
            other.transform.SetParent(transform);
        }
    }

    private void OnTriggerExit2D(Collider2D other)
    {
        if (other.CompareTag("Player"))
        {
            other.GetComponent<Walk>().ToggleScripts(true);
            other.transform.SetParent(null);
            other.GetComponent<Walk>().SpeedMultiplier = 1;
        }
    }
}
