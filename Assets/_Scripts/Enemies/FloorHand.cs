﻿using UnityEngine;

public class FloorHand : MonoBehaviour {

    public Rigidbody2D Arm;

    public Transform Hand;

    public float ChaseForceMultiplier = 150;

    private Transform target;

    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.CompareTag("Player"))
        {
            target = other.transform;
        }
    }
    private void OnTriggerExit2D(Collider2D other)
    {
        if (other.CompareTag("Player"))
        {
            target = null;
        }
    }

    private void Update()
    {
        if(target != null)
        {
            Vector2 direction = target.position - Hand.position;
            direction.Normalize();
            Arm.AddForce(direction * ChaseForceMultiplier, ForceMode2D.Force);
        }
    }
}
