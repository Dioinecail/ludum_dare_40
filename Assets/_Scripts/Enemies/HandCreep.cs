﻿using UnityEngine;

public class HandCreep : MonoBehaviour {

    public float Damage = 1;

    private void OnTriggerStay2D(Collider2D other)
    {
        if(other.CompareTag("Player"))
        {
            other.GetComponent<Walk>().SpeedMultiplier = 0.1f;
            other.GetComponent<PlayerControl>().DisableDodging();
            Debug.LogError("Do damage here");
        }
    }
    private void OnTriggerExit2D(Collider2D other)
    {
        if (other.CompareTag("Player"))
        {
            other.GetComponent<Walk>().SpeedMultiplier = 1;
            other.GetComponent<PlayerControl>().EnableDodging();
        }
    }
}
