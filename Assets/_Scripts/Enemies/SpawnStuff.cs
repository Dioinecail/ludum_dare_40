﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnStuff : MonoBehaviour {

    public GameObject StuffToSpawn;
    public int SpawnChange = 50;

    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.CompareTag("Player") && (Random.Range(0, 100) < SpawnChange))
        {
            Instantiate(StuffToSpawn, transform.position, Quaternion.identity);
            Destroy(gameObject);
        }
    }
}
