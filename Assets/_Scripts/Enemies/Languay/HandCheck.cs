﻿using UnityEngine;

public class HandCheck : MonoBehaviour {

    private Languay Body;
    private HandCheck OtherHand;

    public void SetLanguay(Languay languay, HandCheck otherHand)
    {
        Body = languay;
        OtherHand = otherHand;
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.CompareTag("Player"))
        {
            Body.OnPlayerCatch(transform, other.transform);
            GetComponent<Collider2D>().enabled = false;
            if(OtherHand != null)
                OtherHand.GetComponent<Collider2D>().enabled = false;
        }
    }
}
