﻿using System.Collections;
using UnityEngine;

public class Languay : MonoBehaviour {

    public int HP = 10;

    public Rigidbody2D LeftArm;
    private Transform LeftHand;
    private HandCheck LeftHandCheck;

    public Rigidbody2D RightArm;
    private Transform RightHand;
    private HandCheck RightHandCheck;

    private Rigidbody2D lBody;
    public float BodyCompensationForce = 10;

    public float ChaseForceMultiplier = 3;
    public float Speed;

    public Transform target;

    private LanguayState state = LanguayState.Chase;



    private void Start()
    {
        lBody = GetComponent<Rigidbody2D>();

        LeftHand = LeftArm.transform.GetChild(0);
        LeftHandCheck = LeftHand.GetComponent<HandCheck>();
        LeftHandCheck.SetLanguay(this, RightHandCheck);

        RightHand = RightArm.transform.GetChild(0);
        RightHandCheck = RightHand.GetComponent<HandCheck>();
        RightHandCheck.SetLanguay(this, LeftHandCheck);

        GameManager.onPlayerSpawned += SetTarget;
    }

    private void SetTarget(Transform tgt){
        target = tgt;
    }

    private void OnDestroy()
    {
        GameManager.onPlayerSpawned -= SetTarget;
    }

    private void Update()
    {
        //lBody.AddForce(Vector2.up * BodyCompensationForce, ForceMode2D.Force);
        if (target != null)
        {
            if (state == LanguayState.Chase)
            {
                Vector2 targetDirection = target.position - transform.position;
                lBody.velocity = new Vector2(targetDirection.x > 0 ? Speed : -Speed, lBody.velocity.y);

                Vector2 leftArmDirection = target.position - LeftHand.position;
                leftArmDirection.Normalize();
                Vector2 rightArmDirecion = target.position - RightHand.position;
                rightArmDirecion.Normalize();

                LeftArm.AddForce(leftArmDirection * ChaseForceMultiplier, ForceMode2D.Force);
                RightArm.AddForce(rightArmDirecion * ChaseForceMultiplier, ForceMode2D.Force);
            }
            else
            {
                LeftArm.AddForce(Vector2.up * ChaseForceMultiplier * 2, ForceMode2D.Force);
                RightArm.AddForce(Vector2.up * ChaseForceMultiplier * 2, ForceMode2D.Force);
            }
        }
    }
    public void OnPlayerCatch(Transform catchHand, Transform player)
    {
        state = LanguayState.Caught;
        player.transform.SetParent(catchHand);
        StartCoroutine(CaughtPlayer(player.GetComponent<PlayerControl>()));
    }

    public void RecieveDamage(int amount){
        HP -= amount;
        if(HP <= 0){
            Destroy(gameObject);
        }
    }

    IEnumerator CaughtPlayer(PlayerControl player)
    {
        player.GetComponent<PlayerControl>().OnGotCaught();
        yield return new WaitForSecondsRealtime(3);
        state = LanguayState.Chase;

        bool randomDirection = Random.Range(0, 2) == 1;
        LeftArm.AddForce((randomDirection ? Vector2.right : Vector2.left) * ChaseForceMultiplier, ForceMode2D.Impulse);
        player.GetComponent<Rigidbody2D>().velocity = new Vector2((randomDirection ? ChaseForceMultiplier * 4 : -ChaseForceMultiplier * 4), 0);
        player.GetComponent<Rigidbody2D>().isKinematic = false;
        player.transform.SetParent(null);

        yield return new WaitForSecondsRealtime(1);
        LeftHandCheck.GetComponent<Collider2D>().enabled = true;
        RightHandCheck.GetComponent<Collider2D>().enabled = true;
        player.OnRelease();
    }
}

public enum LanguayState
{
    Chase,
    Caught
}
