﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CanvasesController : MonoBehaviour {

    public Button BossCorrectButton;
    public Button BossWrongButton;
    public Button GrannyYesButton;
    public Slider KnowledgeSlider;
}
