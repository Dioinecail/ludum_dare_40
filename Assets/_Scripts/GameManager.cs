﻿using System;
using UnityEngine;

public class GameManager : MonoBehaviour 
{
    public static event Action<Transform> onPlayerSpawned;

    [SerializeField] private GameObject Level;
    [SerializeField] private GameObject playerPrefab;

    private GameObject player;

    public Vector2 PlayerSpawnPosition;



    [ContextMenu("Spawn")]
    public void SpawnPlayer()
    {
        player = Instantiate(playerPrefab, PlayerSpawnPosition, Quaternion.identity);
        player.transform.position = PlayerSpawnPosition;
        
        onPlayerSpawned?.Invoke(player.transform);
    }

    public void ActivateTrash()
    {
        player.GetComponent<PlayerControl>().NoBoss = false;
    }

    public void DeactivateTrash()
    {
        player.GetComponent<PlayerControl>().NoBoss = true;
    }

    private void OnGameBegin()
    {
        Level.SetActive(true);
		SpawnPlayer();
        CameraFollower.mainCam.SetPersonToFollow(player.transform);
        CameraFollower.mainCam.TransitionStuffback();
		ActivateTrash();
    }

    private void OnEnable()
    {
        CameraFollower.onReadyEvent += OnGameBegin;
    }

    private void OnDisable()
    {
        CameraFollower.onReadyEvent -= OnGameBegin;
    }
}
