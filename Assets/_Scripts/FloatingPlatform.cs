﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FloatingPlatform : MonoBehaviour {

    public float topOffset = 5;
    public float Speed = 5;

    private float originalPosition;

    private void Awake()
    {
        originalPosition = transform.localPosition.y;
    }

    private void Update()
    {
        transform.localPosition = new Vector3(transform.localPosition.x, Mathf.Sin(Time.time * Speed) * topOffset + originalPosition);
    }


    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.CompareTag("Player"))
        {
            other.transform.SetParent(transform);
        }
    }
    private void OnTriggerExit2D(Collider2D other)
    {
        if (other.CompareTag("Player"))
        {
            other.transform.SetParent(null);
        }
    }

    private void OnDrawGizmos()
    {
        Gizmos.color = Color.green;

        Gizmos.DrawWireSphere(transform.position + new Vector3(0, topOffset), 5);
        Gizmos.DrawWireSphere(transform.position + new Vector3(0, -topOffset), 5);
    }
}
