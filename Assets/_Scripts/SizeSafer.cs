﻿using UnityEngine;

public class SizeSafer : MonoBehaviour {
    public float SizeShrink = 0.95f;
    public float DecreaseSizeBy = 0.25f;

    [ContextMenu("Safe")]
    public void PutOnSprite()
    {
        SpriteRenderer rend = GetComponent<SpriteRenderer>();
        BoxCollider2D col = GetComponent<BoxCollider2D>();

        Vector2 size = rend.size;
        col.size = size - new Vector2(DecreaseSizeBy,DecreaseSizeBy);   
    }
}
