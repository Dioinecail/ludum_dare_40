﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class GrannyEvents : MonoBehaviour {

    public Animator Anima;

    public SpriteRenderer Face;

    public Sprite ScaryFace;
    public Sprite NormalFace;

    public GameObject LeftEye;
    public GameObject RightEye;

    public GameObject GrannyCanvas;

    public Button GrannyButton;

    public Text GrannyText;

    public string[] TheVeryBestVariant;
    public string[] BestVariant;
    public string[] GoodVariant;
    public string[] WorstVariant;

    public int CurrentText;

    public void onStartAttack(){
        Face.sprite = ScaryFace;
        LeftEye.SetActive(true);
        RightEye.SetActive(true);


    }

    public void onFinishAttack(){
		Face.sprite = NormalFace;
		LeftEye.SetActive(false);
		RightEye.SetActive(false);


    }

    [ContextMenu("PassText")]
    public void PassText(){
        switch (FindObjectOfType<PlayerControl>().CurrentEnding)
        {
            case Endings.TheVeryBest:
				GrannyText.text = TheVeryBestVariant[CurrentText];
				if (CurrentText < TheVeryBestVariant.Length - 1)
					CurrentText++;
				else
				{
                    SceneManager.LoadScene(0);
				}
				break;
            case Endings.Best:
				GrannyText.text = BestVariant[CurrentText];
				if (CurrentText < BestVariant.Length - 1)
					CurrentText++;
				else
				{
					SceneManager.LoadScene(0);

				}
				break;
            case Endings.Good:
				GrannyText.text = GoodVariant[CurrentText];
				if (CurrentText < GoodVariant.Length - 1)
					CurrentText++;
				else
				{

					SceneManager.LoadScene(0);
				}
				break;
            case Endings.Worst:
				GrannyText.text = WorstVariant[CurrentText];
				if (CurrentText < WorstVariant.Length - 1)
					CurrentText++;
                else{

					SceneManager.LoadScene(0);
				}
				break;
            default:
                break;
		}

        StartCoroutine(YesCooldown());
    }


    [ContextMenu("Action")]
    public void SetAction(){
        Anima.SetTrigger("Action");
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        if(other.CompareTag("Player")){
            // Disable stuff on player's end
            GrannyCanvas.SetActive(true);
            other.GetComponent<PlayerControl>().DisableEverything();
            GrannyButton.interactable = true;
        }
    }

    private IEnumerator YesCooldown(){
        GrannyButton.interactable = false;

        yield return new WaitForSecondsRealtime(1);

        GrannyButton.interactable = true;
    }
}
