﻿using UnityEngine;
using System.Collections;

public class AnimationManager : MonoBehaviour
{

    public const string WALKING = "Hero_Run";
    public const string IDLE = "Hero_Idle";
    public const string JUMP = "Hero_Jump";
    public const string DASH = "Hero_Dash";
    public const string STRIKE = "Hero_Strike";

    private Animator Anima;

    private void Awake()
    {
        Anima = GetComponentInChildren<Animator>();
    }

    public void SetAnimationImmediate(string StateName)
    {
        Anima.Play(StateName);
    }

    public void SetBool(string BoolName, bool BoolValue)
    {
        Anima.SetBool(BoolName, BoolValue);
    }
    public void SetTrigger(string TriggerName)
    {
        Anima.SetTrigger(TriggerName);
    }
    
    public void WaitAnimation(string aName){
        StartCoroutine(Wait(aName));
    }
    private IEnumerator Wait(string animName){
        yield return new WaitForSecondsRealtime(2);

        SetAnimationImmediate(animName);
    }
}
