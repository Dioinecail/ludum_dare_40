﻿using UnityEngine;

public class TrashStuff : MonoBehaviour {

    private Transform target;
    public int HP = 1;
    public float Speed = 50;
    public float KnowledgeToTake = 2;

    private void Awake()
    {
        target = FindObjectOfType<PlayerControl>().transform;
    }

    public void RecieveDamage(int amount){
        HP -= amount;
        if(HP <= 0){
            Destroy(gameObject);
        }
    }

    private void Update()
    {
        if(target != null)
        {
            Vector2 direction = target.position - transform.position;
            direction.Normalize();
            transform.position += (Vector3)direction * Speed * Time.deltaTime;
        }
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.CompareTag("Player"))
        {
            // Do stuff here
            other.GetComponent<PlayerControl>().onKnowledgeChange(-KnowledgeToTake);
            Destroy(gameObject);
        }
    }
}
