﻿using UnityEngine;

public class CollisionState : MonoBehaviour {

    public LayerMask collisionLayer;
    public bool standing;
    public bool onWall;
    public Vector2 bottomPosition;
    public Vector2 leftPosition;
    public Vector2 rightPosition;

    new Transform transform;
    public float collisionRadius;
    public Color collisionColor;

    private InputState inputState;
    private AnimationManager animationManager;



    void Awake()
    {
        inputState = GetComponentInChildren<InputState>();
        animationManager = GetComponent<AnimationManager>();
    }

    void Start()
    {
        transform = base.transform;
    }

    void FixedUpdate(){
        Vector2 pos = bottomPosition + (Vector2)transform.position;

		standing = Physics2D.OverlapCircle(pos, collisionRadius, collisionLayer);
        animationManager.SetBool("OnGround", standing);

	    pos = -(float)inputState.direction * leftPosition + (Vector2)transform.position;

		onWall = Physics2D.OverlapCircle(pos, collisionRadius, collisionLayer);
    }

    void OnDrawGizmos()
    {
        Vector2[] positions =
        {
            bottomPosition + (Vector2)base.transform.position,
            leftPosition + (Vector2)base.transform.position,
            rightPosition + (Vector2)base.transform.position
        };
        foreach (var pos in positions)
		{
			Gizmos.color = collisionColor;
			Gizmos.DrawWireSphere(pos, collisionRadius);
        }

    }

}
