﻿using UnityEngine;

public class Jump : AbstractBehaviour {

    public GameObject JumpParticle;
    public float JumpSpeed = 200;

    public float JumpDelay;
    public float MaxJumpTime = 1;

    public int JumpCount = 1;

    protected float LastJumpTime;
    [SerializeField]
    protected int JumpsRemaining;
    protected bool HoldingJump;

    private AnimationManager animationManager;

    public void Start()
    {
        animationManager = GetComponent<AnimationManager>();
    }

	protected virtual void Update () {
        var jump = inputState.GetButtonValue(inputButtons[0]);
        var holdTime = inputState.GetButtonHoldTime(inputButtons[0]);

        if(collisionState.standing){
            if (jump & holdTime < 0.1f){
                JumpsRemaining = JumpCount;
                OnJump();
            }
        }
        else{
            if (jump && holdTime < MaxJumpTime && JumpsRemaining > 0){
                OnJump();
            }
            else if (HoldingJump){
                OnEndJump();
            }
        }
	}

    protected virtual void OnJump(){
        animationManager.SetAnimationImmediate(AnimationManager.JUMP);
        var velocity = rBody.velocity;
        LastJumpTime = Time.time;
        rBody.velocity = new Vector2(velocity.x, JumpSpeed);
        HoldingJump = true;
        JumpParticle.SetActive(true);
    }
    protected virtual void OnEndJump(){
        animationManager.SetTrigger("Falling");
        var velocity = rBody.velocity;
        velocity.y /= 10;
        rBody.velocity = velocity;
        HoldingJump = false;
        JumpsRemaining--;
		JumpParticle.SetActive(false);
	}
}
