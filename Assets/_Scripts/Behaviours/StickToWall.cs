﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StickToWall : AbstractBehaviour {
    public bool onWallDetected;

    protected float defaultDrag;
    protected float defaultGravityScale;

    void Start()
    {
        defaultGravityScale = rBody.gravityScale;
        defaultDrag = rBody.drag;
    }

    protected virtual void Update(){
        if(collisionState.onWall){
            if(!onWallDetected){
                OnStick();
                onWallDetected = true;
                ToggleScripts(false);
            }
        }else{
            if(onWallDetected){
                OffWall();
                ToggleScripts(true);
                onWallDetected = false;
            }
        }
    }

    protected virtual void OnStick(){
        if(!collisionState.standing && rBody.velocity.y > 0)
        {
            rBody.gravityScale = 0;
            rBody.drag = 100;
        }
    }

    protected virtual void OffWall(){
        if (rBody.gravityScale != defaultGravityScale){
            rBody.gravityScale = defaultGravityScale;
            rBody.drag = defaultDrag;
        }
    }

}
