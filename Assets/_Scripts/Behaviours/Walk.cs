﻿using UnityEngine;

public class Walk : AbstractBehaviour {

    public float speed = 50f;
    public float SpeedMultiplier = 1;
    private AnimationManager animationManager;

    protected override void Awake()
    {
        base.Awake();
        animationManager = GetComponent<AnimationManager>();
    }

    void Update () {
        var right = inputState.GetButtonValue(inputButtons[0]);
        var left = inputState.GetButtonValue(inputButtons[1]);

        if(right || left)
        {
            var tempSpeed = speed;

            var velX = tempSpeed * (float)inputState.direction;

            rBody.velocity = new Vector2(velX * SpeedMultiplier, rBody.velocity.y);
            animationManager.SetBool("Running", true);
            animationManager.SetBool("Idle", false);
        }
        else
        {
            rBody.velocity = new Vector2(0, rBody.velocity.y);
            animationManager.SetBool("Running", false);
            animationManager.SetBool("Idle", true);
        }
    }
}
