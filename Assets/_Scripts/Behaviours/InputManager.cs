﻿using UnityEngine;

public enum Buttons 
{
    Right,
    Left,
    Up,
    Down,
    A,
    B,
    X,
    Y,
    RightBumper,
    LeftBumper
}

public enum Condition
{
    GreaterThan,
    LessThan
}

[System.Serializable]
public class InputAxisState 
{
    public string axisName;
    public float offValue;
    public Buttons button;
    public Condition condition;

    public bool value 
    {
        get 
        {
            var val = Input.GetAxisRaw(axisName);
            switch(condition)
            {
                case Condition.GreaterThan:
                    return val > offValue;
                case Condition.LessThan:
                    return val < offValue;
                default:
                    return false;
            }
        }
    }

}

public class InputManager : MonoBehaviour 
{
    public InputAxisState[] inputs;
    public InputState inputState;



	void Update () 
    {
        foreach (var input in inputs)
        {
            inputState.SetButtonValue(input.button, input.value);
        }
    }
}
