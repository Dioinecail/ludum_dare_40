﻿using UnityEngine;

public class WallSlide : StickToWall {

    public float SlideVelocity = -5;
    public float SlideMultiplier = 5;

	
    protected override void Update()
    {
        base.Update();

        if(onWallDetected){
            var velY = SlideVelocity;

            if(inputState.GetButtonValue(inputButtons[0])){
                velY *= SlideMultiplier;
            }

            rBody.velocity = new Vector2(rBody.velocity.x, velY);
        }
    }

    protected override void OnStick()
    {
        rBody.velocity = Vector2.zero;
    }
    protected override void OffWall()
    {
        // No need to do anything
    } 
}
