﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class AbstractBehaviour : MonoBehaviour {

    public Buttons[] inputButtons;

    public MonoBehaviour[] disableBehaviours;

    protected InputState inputState;
    protected Rigidbody2D rBody;
    protected CollisionState collisionState;

    protected virtual void Awake()
    {
        inputState = GetComponentInChildren<InputState>();
        rBody = GetComponent<Rigidbody2D>();
        collisionState = GetComponent<CollisionState>();
    }

    /// <summary>
    /// Toggles the scripts.
    /// </summary>
    /// <param name="value">If set to <c>true</c> value.</param>
    public virtual void ToggleScripts(bool value){
        for (int i = 0; i < disableBehaviours.Length; i++)
        {
            disableBehaviours[i].enabled = value;
        }
    }

}
