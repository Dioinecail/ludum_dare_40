﻿using UnityEngine;

public class WallJump : AbstractBehaviour {
    public Vector2 JumpVelocity = new Vector2(50, 125);

    public bool JumpingOffWall;
    public float ResetDelay = 0.2f;
    float TimeElapsed;

    void Update()
    {
        if(collisionState.onWall && !collisionState.standing){
            var Jump = inputState.GetButtonValue(inputButtons[0]);
            var holdTime = inputState.GetButtonHoldTime(inputButtons[0]);

            if (Jump && holdTime < 0.005f){
				inputState.direction = inputState.direction == Directions.Right ? Directions.Left : Directions.Right;
				float velX = (float)(inputState.direction);

				rBody.velocity = new Vector2(velX * JumpVelocity.x, JumpVelocity.y);
                ToggleScripts(false);
                JumpingOffWall = true;
            }
        }

        if(JumpingOffWall){
            TimeElapsed += Time.deltaTime;

            if (TimeElapsed > ResetDelay){
				ToggleScripts(true);
				TimeElapsed = 0;
				JumpingOffWall = false;
            }
        }
    }
}
