﻿using UnityEngine;

public class StrikeBehaviour : AbstractBehaviour {

    private AnimationManager animationManager;

    private void Start()
    {
        animationManager = GetComponent<AnimationManager>();
    }

    private void Update()
    {
        var strike = inputState.GetButtonValue(inputButtons[0]);
        var holdTime = inputState.GetButtonHoldTime(inputButtons[0]);
       
        if(strike && holdTime < 0.1f)
        {
            OnStrike();
        }
    }

    private void OnStrike()
    {
        animationManager.SetAnimationImmediate(AnimationManager.STRIKE);
    }

}
