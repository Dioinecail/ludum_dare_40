﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DashBehaviour : AbstractBehaviour 
{
    public float DashSpeed = 150;
    public float DashTime = 0.5f;

    private bool canDash = true;

    private AnimationManager animationManager;



    private void Start()
    {
        animationManager = GetComponent<AnimationManager>();
    }

    private void Update()
    {
        var dash = inputState.GetButtonValue(inputButtons[0]);
        var holdTime = inputState.GetButtonHoldTime(inputButtons[0]);

        if(dash && canDash && holdTime < 0.1f)
        {
            OnDash();
        }
    }

    private void OnDash()
    {
        canDash = false;
        rBody.velocity = new Vector2((float)inputState.direction * DashSpeed, 0);
        StopCoroutine(DashCooldown());
        StartCoroutine(DashCooldown());
        animationManager.SetAnimationImmediate(AnimationManager.DASH);
        ToggleScripts(false);
    }

    private IEnumerator DashCooldown()
    {
        yield return new WaitForSecondsRealtime(DashTime);

        canDash = true;
        ToggleScripts(true);
    }
}