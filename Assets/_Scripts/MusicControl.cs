﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MusicControl : MonoBehaviour 
{
    public static MusicControl Instance;

    [SerializeField] private AudioSource MusicSource;
    [SerializeField] private AudioClip[] musicArray;

    [SerializeField] private bool playOnAwake;



    public void SwitchMusic(int musicNumber)
    {
        MusicSource.clip = musicArray[musicNumber];
        MusicSource.loop = true;
        MusicSource.Play();
    }

    private void Awake()
    {
        Instance = this;

        if (playOnAwake)
            SwitchMusic(0);
    }
}