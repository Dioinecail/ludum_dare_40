﻿namespace Project.Cutscenes
{
    using System.Collections;
    using System.Collections.Generic;
    using UnityEngine;

    [CreateAssetMenu(fileName = "Cutscene_")]
    public class CutsceneAsset : ScriptableObject
    {
        [SerializeField] private CutsceneData[] data;



        public CutsceneData this[int index] { get => data[index]; }
        public int Length { get => data.Length; }
    }

    [System.Serializable]
    public class CutsceneData
    {
        [SerializeField, TextArea(3, 15)] private string text;
        [SerializeField] private AudioClip audio;



        public string Text { get => text; }
        public AudioClip Audio { get => audio; }
    }
}