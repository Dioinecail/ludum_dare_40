﻿namespace Project.Cutscenes
{
    using System;
    using System.Collections;
    using System.Text;
    using UnityEngine;
    using UnityEngine.UI;

    public class CutsceneController : MonoBehaviour
    {
        [SerializeField] private Text targetText;
        [SerializeField] private CutsceneAsset cutsceneData;
        [SerializeField] private AudioSource voiceSource;
        [SerializeField] private InputState cutSceneState;
        [SerializeField] private Buttons confirmButton;

        [SerializeField] private float textSpeed = 5;

        private int CurrentTextNumber;
        private Coroutine currentCoroutine;
        private bool ButtonReady = false;
        private bool isFinishedPlaying = false;



        public void ContinueButton()
        {
            StartCoroutine(ButtonReadyTimer());
            if (CurrentTextNumber < cutsceneData.Length)
            {
                if (targetText.text.Length < cutsceneData[CurrentTextNumber - 1].Text.Length)
                {
                    if (currentCoroutine != null)
                    {
                        StopCoroutine(currentCoroutine);
                        currentCoroutine = null;
                    }

                    targetText.text = cutsceneData[CurrentTextNumber - 1].Text;
                }
                else
                {
                    PlayNextCutsceneData();
                }
            }
            else
            {
                OnCutsceneEnd();
            }
        }

        private void PlayNextCutsceneData()
        {
            if (CurrentTextNumber < cutsceneData.Length)
            {
                PlayAudio(CurrentTextNumber);
                PlayText(CurrentTextNumber);
            }
            else
            {
                OnCutsceneEnd();
            }

            CurrentTextNumber++;
        }

        private void PlayAudio(int currentTextNumber)
        {
            if(voiceSource != null && cutsceneData[currentTextNumber].Audio != null)
                voiceSource.PlayOneShot(cutsceneData[CurrentTextNumber].Audio);
        }

        private void PlayText(int currentTextNumber)
        {
            currentCoroutine = StartCoroutine(PassTextCoroutine(currentTextNumber));
        }

        [ContextMenu("Reset text number")]
        public void ResetTextNumber()
        {
            CurrentTextNumber = 0;
        }

        private IEnumerator PassTextCoroutine(int currentTextNumber)
        {
            string currentText = cutsceneData[currentTextNumber].Text;

            StringBuilder tempStr = new StringBuilder(currentText.Length);

            for (int i = 0; i < currentText.Length; i++)
            {
                if (i > 10)
                    ButtonReady = true;
                tempStr.Append(currentText[i]);
                targetText.text = tempStr.ToString();

                yield return new WaitForSeconds(1 / textSpeed);
            }
        }

        private void Start()
        {
            PlayNextCutsceneData();
        }

        private void Update()
        {
            if (ButtonReady && cutSceneState.GetButtonValue(confirmButton))
                ContinueButton();
        }

        private void OnEnable()
        {
            CameraFollower.PersonToFollow = this.transform;
        }

        private void OnCutsceneEnd()
        {
            if (isFinishedPlaying)
                return;

            isFinishedPlaying = true;

            CameraFollower.mainCam.TransitionStuff();

            StartCoroutine(FadeOut());
        }

        private IEnumerator ButtonReadyTimer()
        {
            ButtonReady = false;

            yield return new WaitForSeconds(1);

            ButtonReady = true;
        }

        private IEnumerator FadeOut()
        {
            yield return new WaitForSeconds(2);

            Destroy(gameObject);
        }
    }
}