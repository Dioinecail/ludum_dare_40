﻿using UnityEngine;
using UnityEngine.UI;

public class PlayerControl : AbstractBehaviour {


	public GameObject SwordStrikePrefab;
	public GameObject[] RandomStuff;
    public float AmountOfKnowledge;
	public float SpawnTimer = 5;
	private int CorrectAnswers;
    public bool NoBoss = true;
	public Vector2 SpawnOffset;
    
	private float spawnTimer;

	private Button CorrectButton;
	private Button WrongButton;
	private Slider KnowledgeSlider;
	private Button GrannyButtonYes;

	public MonoBehaviour[] EveryScript;
	public SpriteRenderer[] Parts;
	
	public Sprite[] NinjaParts;
	public Sprite[] NormalParts;
	
	public AudioSource source;
	
	public AudioClip[] SFX;

    public Endings CurrentEnding = Endings.Worst;

    private void OnEnable()
    {
        CameraFollower.PersonToFollow = this.transform;
        CanvasesController canvasController = FindObjectOfType<CanvasesController>();
		CorrectButton = canvasController.BossCorrectButton;
		WrongButton = canvasController.BossWrongButton;
        GrannyButtonYes = canvasController.GrannyYesButton;
        KnowledgeSlider = canvasController.KnowledgeSlider;
    }
    public void RecieveCorrectAnswer(){
        CorrectAnswers++;

        if(CorrectAnswers == 7 || CorrectAnswers == 8){
            CurrentEnding = Endings.Good;
        }
        else if(CorrectAnswers == 9){
            CurrentEnding = Endings.Best;
        }
    }

    public void HiddenDishes(){
        if (CurrentEnding == Endings.Best){
            CurrentEnding = Endings.TheVeryBest;
        }
    }
    public void StepEvent(){
		PlaySound(0);
    }
    public void JumpEvent(){
        PlaySound(1);
    }

    public void PlaySound(int number){
        source.PlayOneShot(SFX[number]);
    }

    [ContextMenu("Ninja")]
    public void DressupNinja(){
        DressUp(true);
    }
    [ContextMenu("Normal")]
    public void DressupNormal(){
        DressUp(false);
    }

    [ContextMenu("DisableEverything")]
    public void DisableEverything(){
        rBody.velocity = Vector2.zero;
        GetComponent<AnimationManager>().SetBool("Running", false);
        GetComponent<AnimationManager>().WaitAnimation(AnimationManager.IDLE);

        foreach (var mono in EveryScript)
        {
            mono.enabled = false;
        }
    }

    public void DressUp(bool ninja){
        for (int i = 0; i < Parts.Length - 4; i++)
        {
            Parts[i].sprite = ninja ? NinjaParts[i] : NormalParts[i];
        }

            Parts[Parts.Length - 1].enabled = ninja;
            Parts[Parts.Length - 2].enabled = ninja;
            Parts[Parts.Length - 3].enabled = ninja;
            Parts[Parts.Length - 4].enabled = ninja;

    }

    public void Strike()
    {
        Instantiate(SwordStrikePrefab, transform.position + (float)(inputState.direction) * new Vector3(1,0), Quaternion.identity, transform);
        spawnTimer = SpawnTimer;
    }
    private void Update()
    {
        if(spawnTimer > 0)
        {
            spawnTimer -= Time.deltaTime;
        }
        else if(NoBoss)
        {
            spawnTimer = SpawnTimer;
            SpawnRandomShit();
        }

        var yes = inputState.GetButtonValue(inputButtons[0]);
        var no = inputState.GetButtonValue(inputButtons[1]);

        if(yes && CorrectButton.interactable){
            CorrectButton.onClick.Invoke();
        }

        if(yes && GrannyButtonYes.interactable){
            GrannyButtonYes.onClick.Invoke();
        }

        if(no && WrongButton.interactable){
            WrongButton.onClick.Invoke();
        }
    }

    public void onKnowledgeChange(float amount){
        AmountOfKnowledge += amount;
        if (AmountOfKnowledge >= 10){
            AmountOfKnowledge = 10;
        }
        else if (AmountOfKnowledge < 0){
            AmountOfKnowledge = 0;
        }

        KnowledgeSlider.value = AmountOfKnowledge;
    }

    private void OnDrawGizmosSelected()
    {
        Gizmos.color = Color.yellow;
        Gizmos.DrawWireCube(transform.position, SpawnOffset * 2);
    }
    private void SpawnRandomShit()
    {
        int rnd = Random.Range(0, RandomStuff.Length);
        float randomOffset = Random.Range(-SpawnOffset.x, SpawnOffset.x);

        Vector2 randomOffsetVector;
        if(Random.Range(0,2) > 0)
        {
            if (Random.Range(0, 2) > 0)
            {
                randomOffsetVector = new Vector2(randomOffset, SpawnOffset.y);
            }
            else
                randomOffsetVector = new Vector2(randomOffset, -SpawnOffset.y);
        }
        else
        {
            if (Random.Range(0, 2) > 0)
            {
                randomOffsetVector = new Vector2(SpawnOffset.x, randomOffset);
            }
            else
                randomOffsetVector = new Vector2(-SpawnOffset.x, randomOffset);
        }

        Instantiate(RandomStuff[rnd], (Vector2)transform.position + randomOffsetVector, Quaternion.identity);
    }

    public void DisableDodging()
    {
        GetComponent<Jump>().enabled = false;
        GetComponent<DashBehaviour>().enabled = false;
    }
    public void EnableDodging()
    {
        GetComponent<Jump>().enabled = true;
        GetComponent<DashBehaviour>().enabled = true;
    }

    public void OnGotCaught()
    {
        rBody.isKinematic = true;
        rBody.velocity = Vector2.zero;
        ToggleScripts(false);
    }

    public void OnRelease()
    {
        ToggleScripts(true);
        transform.rotation = new Quaternion(0, 0, 0, 0);
    }
}

public enum Endings{
    TheVeryBest,
    Best,
    Good,
    Worst
}